﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CakeShop.Models
{
    public class Pizza
    {
        public int recPizzaID { get; set; }
        public string PizzaType { get; set; }
        public string PizzaToppings { get; set; }

        public string location { get; set; }
    }
}