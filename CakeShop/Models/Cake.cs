﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CakeShop.Models
{
    public class Cake
    {
        public int recCakeID { get; set; }
        public string CakeName { get; set; }
        public string location { get; set; }
    }
}