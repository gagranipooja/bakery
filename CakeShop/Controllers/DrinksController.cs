﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CakeShop.Models;


namespace CakeShop.Controllers
{
    public class DrinksController : Controller
    {
        // GET: Drinks
        public ActionResult Index()
        {
            Drinks Drinks = new Drinks();
            Drinks.recDrinksID = 1;
            Drinks.DrinkName = "CoCo-Cola";
            Drinks.location = "USA";

            return View(Drinks);
        }
    }
}