﻿using CakeShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CakeShop.Controllers
{
    public class PizzaController : Controller
    {
        // GET: Pizza
        public ActionResult Index()
        {

            Pizza pizza = new Pizza();
            pizza.recPizzaID = 1;
            pizza.PizzaType = "New York Style Pizza";
            pizza.PizzaToppings = "Cheese";
            pizza.location = "India";

            return View(pizza);

           
        }
    }
}