﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CakeShop.Models;

namespace CakeShop.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Cake cake = new Cake();
            cake.recCakeID = 1;
            cake.CakeName = "Apple Dutch Cake";
            cake.location = "Holland";


            return View(cake);
        }
    }
}